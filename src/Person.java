
public class Person implements Comparable, Taxable{
	private String name ;
	private double income ;
	
	Person(String name, double income){
		this.name = name;
		this.income = income;
	}

	@Override
	public int compareTo(Object otherObject) {
		Person other = (Person) otherObject;
		if(income < other.income) return -1 ;
		else if(income > other.income) return 1 ;
		return 0 ;
	}
	
	public String toString(){
		return name + "Income:" + income + ": Tax:" + getTax();
	}

	@Override
	public double getTax() {
		if(income < 300001) return income*0.05 ;
		return (300000*0.05)+(income-300000)*0.1;
	}
	
	
}
