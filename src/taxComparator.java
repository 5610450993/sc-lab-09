import java.util.Comparator;


public class taxComparator implements Comparator<Taxable>{

	@Override
	public int compare(Taxable o1, Taxable o2) {
		if(o1.getTax() < o2.getTax()) return -1 ;
		else if(o1.getTax() > o2.getTax()) return 1 ;
		return 0 ;
	}


}
