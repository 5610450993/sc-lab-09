import java.util.Comparator;


public class Company implements Taxable{
	private String company ;
	private double income ;
	private double expense ;
	private double profit ;
	
	Company(String company, double income, double expense){
		this.company = company;
		this.income = income;
		this.expense = expense;
		this.profit = income-expense;
	}
	
	public double getIncome(){
		return income;
	}
	
	public double getExpense(){
		return expense;
	}
	
	public double getProfit(){
		return profit;
	}
	
	public String toString(){
		return company + ": Income:" + income + ": Expense:" + expense + ": Profit:" + profit + ": Tax:" + getTax();
	}

	@Override
	public double getTax() {
		return (income-expense)*0.3;
	}
}
