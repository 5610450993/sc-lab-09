package TreeTraversal;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class InOrderTraversal implements Traversal{

	@Override
	public List<Node> traverse(Node node) {
		List<Node> in = new ArrayList<Node>();
		if(node == null)return in;
		if(node.getLeft() != null) in.addAll(traverse(node.getLeft()));
		in.add(node);
		if(node.getRight() != null) in.addAll(traverse(node.getRight()));
		return in;
	}

}
