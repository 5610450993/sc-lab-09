package TreeTraversal;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class PreOrderTraversal implements Traversal{

	@Override
	public List<Node> traverse(Node node) {
		List<Node> pre = new ArrayList<Node>();
		if(node == null)return pre;
		pre.add(node);
		if(node.getLeft() != null) pre.addAll(traverse(node.getLeft()));
		if(node.getRight() != null) pre.addAll(traverse(node.getRight()));
		return pre;
	}

}
