package TreeTraversal;

import java.util.List;

public class ReportConsole {
	
	public void display(Node root, Traversal traver){
		List<Node> node = traver.traverse(root);
		for(Node n: node) System.out.print(n.getValue()+" ");
	}
}
