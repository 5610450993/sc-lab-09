package TreeTraversal;

public class TraverseTest {
	
	public static void main(String[] args){
		Node c = new Node("C", null, null);
		Node e = new Node("E", null, null);
		Node d = new Node("D", c, e);
		Node a = new Node("A", null, null);
		Node b = new Node("B", a, d);
		Node h = new Node("H", null, null);
		Node i = new Node("I", null, null);
		Node g = new Node("G", null, i);
		Node f = new Node("F", b, g);
		
		ReportConsole console = new ReportConsole();
		System.out.print("<----Traversal---->\nPreOrderTraversal:\t");
		console.display(f, new PreOrderTraversal());
		System.out.print("\nInOrderTraversal:\t");
		console.display(f, new InOrderTraversal());
		System.out.print("\nPostOrderTraversal:\t");
		console.display(f, new PostOrderTraversal());
	}
}
