package TreeTraversal;

import java.util.ArrayList;
import java.util.List;

public class PostOrderTraversal implements Traversal{

	@Override
	public List<Node> traverse(Node node) {
		List<Node> post = new ArrayList<Node>();
		if(node == null)return post;
		if(node.getLeft() != null) post.addAll(traverse(node.getLeft()));
		if(node.getRight() != null) post.addAll(traverse(node.getRight()));
		post.add(node);
		return post;
	}

}
