import java.util.Comparator;


public class ExpenseComparator implements Comparator<Company>{

	@Override
	public int compare(Company o1, Company o2) {
		if(o1.getExpense() < o2.getExpense()) return -1 ;
		else if(o1.getExpense() > o2.getExpense()) return 1 ;
		return 0 ;
	}


}
