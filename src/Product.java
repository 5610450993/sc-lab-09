
public class Product implements Comparable, Taxable{
	private String product ;
	private double price ;

	Product(String product, double price){
		this.product = product;
		this.price = price;
	}

	@Override
	public int compareTo(Object otherObject) {
		Product other = (Product) otherObject;
		if(price < other.price) return -1 ;
		else if(price > other.price) return 1 ;
		return 0 ;
	}
	
	public String toString(){
		return product + "Price:" + price + ": Tax:" + getTax();
	}

	@Override
	public double getTax() {
		return price*0.07;
	}
}
