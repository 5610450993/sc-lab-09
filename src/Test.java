import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Test {

	public static void main(String[] args) {
		Test t = new Test();
		t.testPer();
		t.testPro();
		t.testCom();
		t.testTaxable();
	}
	
	public void testPer(){
		ArrayList<Person> per = new ArrayList<Person>();
		per.add(new Person("A", 500000));
		per.add(new Person("B", 250000));
		per.add(new Person("C", 375000));
		System.out.println("<----------Person---------->\nBefore\n" + per + "\n");
		Collections.sort(per);
		System.out.println("After\n" + per + "\n");
	}
	
	public void testPro(){
		ArrayList<Product> pro = new ArrayList<Product>();
		pro.add(new Product("A", 500000));
		pro.add(new Product("B", 250000));
		pro.add(new Product("C", 375000));
		System.out.println("<----------Product---------->\nBefore\n" + pro + "\n");
		Collections.sort(pro);
		System.out.println("After\n" + pro + "\n");
	}
	
	public void testCom(){
		ArrayList<Company> com = new ArrayList<Company>();
		com.add(new Company("A", 40000 , 4000));
		com.add(new Company("B", 100000 , 10000));
		com.add(new Company("C", 50000 , 500));
		System.out.println("<----------Company---------->\nBefore\n" + com + "\n");
		Collections.sort(com, new EarningComparator());
		System.out.println("After Sort Income\n" + com + "\n");
		Collections.sort(com, new ExpenseComparator());
		System.out.println("After Sort Expense\n" + com + "\n");
		Collections.sort(com, new ProfitComparator());
		System.out.println("After Sort Profit\n" + com + "\n");
	}
	
	public void testTaxable(){
		ArrayList<Person> per = new ArrayList<Person>();
		per.add(new Person("A", 500000));
		per.add(new Person("B", 250000));
		System.out.println("<----------Person---------->\nBefore\n" + per + "\n");
		Collections.sort(per, new taxComparator());
		System.out.println("After Sort Tax\n" + per + "\n");
		
		ArrayList<Product> pro = new ArrayList<Product>();
		pro.add(new Product("A", 50000));
		pro.add(new Product("B", 5));
		System.out.println("<----------Product---------->\nBefore\n" + pro + "\n");
		Collections.sort(pro, new taxComparator());
		System.out.println("After Sort Tax\n" + pro + "\n");
		
		ArrayList<Company> com = new ArrayList<Company>();
		com.add(new Company("A", 40000 , 4000));
		com.add(new Company("B", 100000 , 10000));
		System.out.println("<----------Company---------->\nBefore\n" + com + "\n");
		Collections.sort(com, new taxComparator());
		System.out.println("After Sort Tax\n" + com + "\n");
		
		ArrayList<Taxable> taxAll = new ArrayList<Taxable>();
		taxAll.add(new Person("A", 500000));
		taxAll.add(new Person("B", 250000));
		taxAll.add(new Company("A", 40000 , 4000));
		taxAll.add(new Company("B", 100000 , 10000));
		taxAll.add(new Product("A", 50000));
		taxAll.add(new Product("B", 5));
		System.out.println("<----------All---------->\nBefore\n" + taxAll + "\n");
		Collections.sort(taxAll, new taxComparator());
		System.out.println("After Sort TaxAll\n" + taxAll + "\n");
	}

}
