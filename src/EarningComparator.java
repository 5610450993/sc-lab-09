import java.util.Comparator;

public class EarningComparator implements Comparator<Company>{

	@Override
	public int compare(Company o1, Company o2) {
		if(o1.getIncome() < o2.getIncome()) return -1 ;
		else if(o1.getIncome() > o2.getIncome()) return 1 ;
		return 0 ;
	}

}
